node-debug (3.1.0-2) UNRELEASED; urgency=medium

  * Update Vcs fields for migration to https://salsa.debian.org/

 -- Paolo Greppi <paolo.greppi@libpf.com>  Thu, 05 Apr 2018 15:28:37 +0200

node-debug (3.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards versions
  * Unbundle node-ms and depend on it (Closes: #836205)
  * No need to patch away the build-dependency on sinon-chai since
    node-sinon-chai is now in the archive
  * Also produce a libjs-debug binary
  * Patch the provided browser example to use library from
    /usr/share/javascript
  * Provide another example for using this library in the browser

 -- Paolo Greppi <paolo.greppi@libpf.com>  Mon, 27 Nov 2017 13:32:52 +0100

node-debug (2.5.1-2) unstable; urgency=medium

  * Team upload.
  * Install node.js file

 -- Pirate Praveen <praveen@debian.org>  Fri, 23 Jun 2017 00:27:25 +0530

node-debug (2.5.1-1) unstable; urgency=medium

  [ Paolo Greppi ]
  * Team upload.
  * New upstream release.
  * Standards-Version 3.9.8
  * debhelper 9
  * Update Vcs-* fields
  * Update copyright & uploaders
  * Update patch that bundles ms 0.7.2
  * Patch away the dependency on sinon-chai
  * Enable build-time tests
  * Enable autopkgtests
  * Build-Depends chai, node-sinon for tests

  [ Jérémy Lal ]
  * Install all src dir

 -- Paolo Greppi <paolo.greppi@libpf.com>  Fri, 23 Dec 2016 23:30:45 +0100

node-debug (2.1.0+dfsg-2) unstable; urgency=medium

  * Ensure browser.js is installed for browserify.

 -- Daniel Pocock <daniel@pocock.pro>  Fri, 03 Jul 2015 13:44:44 +0200

node-debug (2.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version 3.9.6
  * autopkgtest: test ms module can be required as humanize
  * debian/copyright: update year copyright

 -- Leo Iannacone <l3on@ubuntu.com>  Thu, 16 Oct 2014 09:45:52 +0200

node-debug (2.0.0+dfsg-1) unstable; urgency=medium

  * Imported Upstream version 2.0.0
  * Repackaged to exclude pre-compiled files
    - d/watch: add dversionmangle
  * Add autopkgtest to require debug module
  * Bundle module "ms" as patch, it is exported as "humanize":
    - d/install: install ms module locally
    - d/copyright: updated with ms module copyright
    - d/rules: override dh_auto_clean in order to not remove
      node_modules directory
  * d/install: update to new install correct files
  * d/copyright: add upstream-contact

 -- Leo Iannacone <l3on@ubuntu.com>  Sat, 04 Oct 2014 16:00:20 +0200

node-debug (0.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Fix debian watch, now refers to project homepage.
  * debian/control:
    + update homepage url
    + update Vcs-* fields
    + bump standards version
    + added dh-buildinfo and nodejs as build-depends
    + added Leo Iannacone as uploader
    + improved long description
  * debian/copyright:
    + set Expat license instead of MIT
  * debian/install:
    + install package.json in node path
  * debian/rules:
    + override dh_auto_build to fix FTBFS
    + keep original name of the upstream changelog

 -- Leo Iannacone <l3on@ubuntu.com>  Thu, 24 Apr 2014 13:21:46 +0200

node-debug (0.6.0-1) unstable; urgency=low

  * Initial release

 -- David Paleino <dapal@debian.org>  Sun, 15 Apr 2012 00:40:50 +0200
